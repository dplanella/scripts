#!/usr/bin/env python3

""" Split owner and slack group from tech stack file.

This module parses GitLab's tech stack file, extracts the owner group
and Slack channel from the originally combined group_owner_slack_channel
key and adds them to dedicated keys that can then be parsed programmatically.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

import re
import sys
# We use this YAML module instead of the standard PyYaml
# as it allows us to preserve the quote style
from ruamel.yaml import YAML
from collections import OrderedDict

tech_stack_with_owner = []
owner_pattern = re.compile(r"((\b[\w]+\b ){1,4})(\(Group Owner\))",
                           re.IGNORECASE)
slack_pattern = re.compile(r"#\b[\w-]+\b")
ordered_keys = [
    'title',
    'team_member_baseline_entitlement',
    'subprocessor',
    'description',
    'access_to',
    'provisioner',
    'deprovisioner',
    'group_owner',
    'group_owner_slack_channel',
    'business_owner',
    'technical_owner',
    'data_classification',
    'integration_method',
    'need_move_to_okta',
    'critical_systems_tier',
    'in_sox_scope',
    'date_added_to_sox_scope',
    'collected_data',
    'processing_customer_data',
    'employee_or_customer_facing_app',
    'notes',
    'saas',
]


def my_represent_none(self, data):
    '''
    Emit "null" for mappings which are also "null" on the
    original YAML file
    '''
    return self.represent_scalar(u'tag:yaml.org,2002:null', u'null')


yaml = YAML()

# Set up the YAML object

# Keep "null" as "null" in the output
yaml.representer.add_representer(type(None), my_represent_none)

# We preserve the quotes style from the original
# to keep changes to the minimum
yaml.preserve_quotes = True

# Keep the key order as the original
yaml.sort_keys = False

# It is not possible to keep the indentation of flow scalars,
# particularly as the original file mixes different types.
# While it would be desirable to keep the indentation as the
# original to minimize the diff, a compromise is to make all
# of them single line, to keep the diff to a minimum.
# On a subsequent iteration, they can all be reformatted
# consistently at once if necessary.
yaml.width = float('inf')

# We want block flow style as the original
yaml.default_flow_style = False

with open('tech_stack.yml', 'r') as stream:
    tech_stack_dict = yaml.load(stream)

for app in tech_stack_dict:
    owner = ""
    slack = ""
    group_owner_slack_channel = app['group_owner_slack_channel']
    # Extract the owner group and owner slack channel from the
    # group_owner_slack_channel key 
    if group_owner_slack_channel:
        owner_match = re.search(owner_pattern, group_owner_slack_channel)
        slack_match = re.search(slack_pattern, group_owner_slack_channel)

        if owner_match:
            owner = owner_match.group(1).rstrip()
        if slack_match:
            slack = slack_match.group().rstrip()

    # Create a new group_owner key and assign it the owner group
    # Put only the slack channel on the group_owner_slack_channel key
    app['group_owner'] = owner
    app['group_owner_slack_channel'] = slack

    # We need keys to be in the same order as the original to
    # minimize the diff, so we put them in the given order on
    # an OrderedDict
    try:
        list_of_tuples = [(key, app[key]) for key in ordered_keys]
    except KeyError as exc:
        print(f"Key {exc} does not exist in app {app['title']}")
        sys.exit(1)

    tech_stack_with_owner.append(dict(OrderedDict(list_of_tuples)))

    # Printing each entry within a loop is the only way to keep
    # the original file's format with a line break to separate
    # entries (as opposed to printing the full list at once, outside
    # of the loop)
    yaml.dump([dict(OrderedDict(list_of_tuples))],
                    sys.stdout,
                    )
    # Print the newline after each entry
    print()
